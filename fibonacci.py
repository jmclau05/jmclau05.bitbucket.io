'''@file fibonacci.py
Contains a function to find Fibonacci numbers by index
and a simple interface to run as a standalone program.
'''


def fib(idx):
    # Finds the Fibbonacci number with the index idx.
    # idx must be a nonnegative integer.
    # The 0th and 1st Fibonacci Numbers are 0 and 1 by definition.
    if idx == 0:
        return 0
    elif idx == 1:
        return 1
    else:
        # Keep finding the next numbers in the sequence, until
        # the desired index is reached (bottom up method).
        a = 0
        b = 1
        for i in range(idx - 1):
            c = a + b
            a = b
            b = c
        return c


if __name__ == '__main__':
    while True:
        # Collect input and verify that it is a nonnegative integer.
        try:
            idx = int(input('Choose an index: '))
        except:
            print('The index must be an integer!')
            continue
        if idx < 0:
            print('The index must be nonnegative!')
            continue
        else:
            print('Fibonacci number at '
                  'index {:} is {:}.'.format(idx, fib(idx)))
            # Handle entering more indexes or quitting the program
            q = input('Enter q to quit or press enter to continue: ')
            if q == 'q':
                break
            else:
                continue
