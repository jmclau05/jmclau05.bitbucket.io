var dir_d78a8c735146dce50c791d3c7c30ff81 =
[
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847.py", "lab4_2DRV8847_8py.html", "lab4_2DRV8847_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "lab4_2main_8py.html", null ],
    [ "shares.py", "lab4_2shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "lab4_2task__controller_8py.html", [
      [ "task_controller.Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_encoder.py", "lab4_2task__encoder_8py.html", [
      [ "task_encoder.Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_user.py", "lab4_2task__user_8py.html", "lab4_2task__user_8py" ]
];