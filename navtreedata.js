/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Jackson McLaughlin ME 305", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab 0: Fibonacci Sequence", "index.html#sec_fib", null ],
    [ "Lab 1: Light Show", "index.html#sec_light", null ],
    [ "Lab 2: Encoder Display", "index.html#sec_enc", null ],
    [ "Lab 3: Motor test", "index.html#sec_mot", null ],
    [ "Lab 4: Closed Loop Control", "index.html#sec_clc", null ],
    [ "Lab 5: BNO055 IMU Driver", "index.html#sec_imu", null ],
    [ "Homework 2 & 3: Balancing platform model and simulation", "index.html#sec_hw2", null ],
    [ "Final Project: Balancing Ball Platform", "index.html#Final", null ],
    [ "Final Ball Balancing Platform", "final.html", [
      [ "Term Project", "final.html#sec_report", null ],
      [ "Goals and Hardware", "final.html#sec_goals", null ],
      [ "Firmware Design", "final.html#sec_fw_design", null ],
      [ "Code Organization", "final.html#sec_code_organization", null ],
      [ "System Response Plots", "final.html#sec_resp_plots", null ],
      [ "Video", "final.html#vid", null ]
    ] ],
    [ "Homework 2 and 3", "term.html", null ],
    [ "Closed Loop Control", "tuning.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';