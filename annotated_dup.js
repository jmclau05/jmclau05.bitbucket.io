var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "closedloop", null, [
      [ "ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_data_collect", null, [
      [ "Task_Data_Collect", "classtask__data__collect_1_1Task__Data__Collect.html", "classtask__data__collect_1_1Task__Data__Collect" ]
    ] ],
    [ "task_data_collect io", null, [
      [ "Task_Data_Collect", "classtask__data__collect_01io_1_1Task__Data__Collect.html", "classtask__data__collect_01io_1_1Task__Data__Collect" ]
    ] ],
    [ "task_data_collect printing", null, [
      [ "Task_Data_Collect", "classtask__data__collect_01printing_1_1Task__Data__Collect.html", "classtask__data__collect_01printing_1_1Task__Data__Collect" ]
    ] ],
    [ "task_data_collect_complicated", null, [
      [ "Task_Data_Collect", "classtask__data__collect__complicated_1_1Task__Data__Collect.html", "classtask__data__collect__complicated_1_1Task__Data__Collect" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ],
      [ "Task_Encoder2", "classtask__encoder_1_1Task__Encoder2.html", "classtask__encoder_1_1Task__Encoder2" ],
      [ "Task_Encoder3", "classtask__encoder_1_1Task__Encoder3.html", "classtask__encoder_1_1Task__Encoder3" ]
    ] ],
    [ "task_imu", null, [
      [ "Task_IMU", "classtask__imu_1_1Task__IMU.html", "classtask__imu_1_1Task__IMU" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_panel", null, [
      [ "Task_Panel", "classtask__panel_1_1Task__Panel.html", "classtask__panel_1_1Task__Panel" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ],
      [ "Task_User2", "classtask__user_1_1Task__User2.html", "classtask__user_1_1Task__User2" ],
      [ "Task_User3", "classtask__user_1_1Task__User3.html", "classtask__user_1_1Task__User3" ]
    ] ],
    [ "touchpanel", null, [
      [ "TouchPanel", "classtouchpanel_1_1TouchPanel.html", "classtouchpanel_1_1TouchPanel" ]
    ] ]
];