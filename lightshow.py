''' @file           main.py
    @brief          Implements light show using a state machine
    @details        This file will implents a light show, with three choices of
                    pattern: 1. Square Wave 2. Sine Wave 3. Sawtooth Wave.
                    It uses the built in LED LD3 and blue user button B1 on the 
                    Nucleo. The user can choose the patterns by pushing the button.
                    This is accomplished using a Finite State Machine.
    
    @author         Jackson McLaughlin
    @author         Tori Bornino
    @date           October 2, 2021
'''

import utime
import pyb
import math


def onButtonPressFCN(IRQ_src):
    '''@brief       Sets buttonFlag True when button is pushed.
       @details     Used as the callback function for the interrupt.
                    Records that the button was pressed.
       @param       IRQ_src Source of the interrupt. Required by MicroPython
                            for callback functions, but unused.
    '''
    # @brief       Global boolean variable for when button has been pressed.
    #  @details     buttonFlag is a global boolean that is used to indicate
    #               whether the user button B1 has been pressed recently.
    #               It is set True when the button is pressed and reset to
    #               False when the pattern is changed.
    #
    global buttonFlag
    buttonFlag = True


def resetTimer():
    '''@brief       Resets the reference point of the timer.
       @details     Uses utime.ticks_ms() to record a start time, which is used
                    as a reference for updating the timer.
       @return      Reference start time for timer, in milliseconds.
    '''
    startTime = utime.ticks_ms()
    return startTime


def updateTimer(startTime):
    '''@brief       Updates the current time.
       @details     Uses utime.ticks_ms() and utime.ticks_diff() to record the
                    current time and then subtract the reference time.
       @param       startTime   Reference time for when current pattern started.
       @return      Elapsed time since pattern started.
    '''
    stopTime = utime.ticks_ms()
    elapsedTime = utime.ticks_diff(stopTime, startTime)
    return elapsedTime


def updateSqw(elapsedTime):
    '''@brief       Computes the brightness for the square wave pattern.
       @details     Uses the elapsed time to determine LED brightness.
                    0% brightness for first half of period, 100% for second
                    half. Period is 1000 milliseconds.
       @param       elapsedTime Elapsed time since pattern started.
       @return      LED brightness in percent.
    '''

    return 100 * (elapsedTime % 1000 > 500)


def updateSw(elapsedTime):
    '''@brief       Computes the brightness for the sine wave pattern.
       @details     Uses the elapsed time to determine LED brightness.
                    The pattern is a sine wave oscillating between 0% and 100%
                    brightness with a period of 10000 millliseconds.
       @param       elapsedTime Elapsed time since pattern started.
       @return      LED brightness in percent.
    '''
    return 50 + 50 * math.sin((2 * math.pi / 10000) * (elapsedTime % 10000))


def updateStw(elapsedTime):
    '''@brief       Computes the brightness for the sine wave pattern.
       @details     Uses the elapsed time to determine LED brightness.
                    The pattern is a sawtooth wave ramping from 0% to 100%
                    brightness with a period of 1000 millliseconds.
       @param       elapsedTime Elapsed time since pattern started.
       @return      LED brightness in percent.
    '''
    return (elapsedTime % 1000) / 10


if __name__ == '__main__':
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq=20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

    # @brief       Global integer variable for what state program is in.
    #  @details     See state transition diagram for the meaning of each state.
    #
    state = 0
    # @brief       global integer variable for LED brightness.
    #  @details     PWM brightness of LED LD3, in percent.
    #
    brightness = 0

    global buttonFlag
    buttonFlag = False
    welcomed = False

    while(True):

        try:

            if (state == 0):
                print("Initializing...")
                state = 1  # Transition to state 1

            elif (state == 1):
                if welcomed == False:
                    print("Push button B1 (blue) to start light show.")
                    print("Push again to cycle through patterns.")
                    welcomed = True
                if (buttonFlag == True):
                    buttonFlag = False
                    # @brief       Global integer variable for timer reference
                    #  @details     Millisecond tick value that should be
                    #               subtracted from the current time when
                    #               figuring out the time elapsed since pattern
                    #               was started.
                    #
                    startTime = resetTimer()
                    print("Square Wave Pattern Selected")
                    state = 2
                    continue

            elif (state == 2):
                if (buttonFlag == True):
                    buttonFlag = False
                    startTime = resetTimer()
                    state = 3
                    print("Sine Wave Pattern Selected")
                    continue
                else:
                    # @brief       Time since current pattern started
                    #  @details     Millisecond tick value since the current
                    #               pattern was started (last button push)
                    #
                    elapsedTime = updateTimer(startTime)
                    brightness = updateSqw(elapsedTime)
                    t2ch1.pulse_width_percent(brightness)

            elif (state == 3):
                if (buttonFlag == True):
                    buttonFlag = False
                    startTime = resetTimer()
                    state = 4
                    print("Sawtooth Wave Pattern Selected")
                    continue
                else:
                    elapsedTime = updateTimer(startTime)
                    brightness = updateSw(elapsedTime)
                    t2ch1.pulse_width_percent(brightness)

            elif (state == 4):
                if (buttonFlag == True):
                    buttonFlag = False
                    startTime = resetTimer()
                    state = 2
                    print("Square Wave Pattern Selected")
                    continue
                else:
                    elapsedTime = updateTimer(startTime)
                    brightness = updateStw(elapsedTime)
                    t2ch1.pulse_width_percent(brightness)

        except KeyboardInterrupt:
            break

    print('Program Termination')