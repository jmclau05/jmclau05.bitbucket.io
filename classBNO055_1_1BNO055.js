var classBNO055_1_1BNO055 =
[
    [ "__init__", "classBNO055_1_1BNO055.html#aab42d5cc298fe5da7679ac7b91b103a0", null ],
    [ "__init__", "classBNO055_1_1BNO055.html#aab42d5cc298fe5da7679ac7b91b103a0", null ],
    [ "get_calibration_coefficients", "classBNO055_1_1BNO055.html#aee467e8cea226178a9f76b90839aec8a", null ],
    [ "get_calibration_coefficients", "classBNO055_1_1BNO055.html#aee467e8cea226178a9f76b90839aec8a", null ],
    [ "get_calibration_status", "classBNO055_1_1BNO055.html#aaa9dfe166380cf324009a27060cf00c7", null ],
    [ "get_calibration_status", "classBNO055_1_1BNO055.html#aaa9dfe166380cf324009a27060cf00c7", null ],
    [ "read_angular_velocity", "classBNO055_1_1BNO055.html#a219fa1f4c5771a91449f75b1b7bb0911", null ],
    [ "read_angular_velocity", "classBNO055_1_1BNO055.html#a219fa1f4c5771a91449f75b1b7bb0911", null ],
    [ "read_euler_angles", "classBNO055_1_1BNO055.html#a9dad2b1fb181dec25f5b15f0bf509e1e", null ],
    [ "read_euler_angles", "classBNO055_1_1BNO055.html#a9dad2b1fb181dec25f5b15f0bf509e1e", null ],
    [ "set_calibration_coefficients", "classBNO055_1_1BNO055.html#ad9524187987391789de975d5def85364", null ],
    [ "set_calibration_coefficients", "classBNO055_1_1BNO055.html#ad9524187987391789de975d5def85364", null ],
    [ "set_operating_mode", "classBNO055_1_1BNO055.html#a8f40c03bdcb8894b2a789c28d089099d", null ],
    [ "set_operating_mode", "classBNO055_1_1BNO055.html#a8f40c03bdcb8894b2a789c28d089099d", null ]
];